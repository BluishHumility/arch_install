#!/bin/bash

# Set luks_install
luks_install="luks_install_placeholder"

# Set username
username="username_placeholder"

# Set password
password="password_placeholder"

# Set hostname
machine_hostname="hostname_placeholder"

# Set btrfs partition
btrfs_partition="btrfs_partition_placeholder"

# Get UUID for each partition
efi_uuid=$(findmnt -no UUID /efi)
if [[ "$luks_install" == "y" ]]; then
    boot_uuid=$(findmnt -no UUID /boot)
    btrfs_uuid=$(blkid -s UUID -o value /dev/mapper/root)
else
    btrfs_uuid=$(findmnt -no UUID /)
fi

# Generate /etc/fstab
echo "Generating /etc/fstab..."
cat <<FSTAB >> /etc/fstab
UUID=${efi_uuid}  	/efi 		vfat 	defaults,noatime 0 2
$(if [[ "$luks_install" == "y" ]]; then
    cat <<EOF
UUID=${boot_uuid} 	/boot 		btrfs   subvol=arch-i3_boot,noatime,compress=zstd 0 0
EOF
fi)
UUID=${btrfs_uuid} 	/  			btrfs   subvol=arch-i3,noatime,compress=zstd 0 0
UUID=${btrfs_uuid} 	/home 		btrfs   subvol=arch-i3_home,noatime,compress=zstd 0 0
UUID=${btrfs_uuid} 	/var/cache 	btrfs   subvol=arch-i3_cache,noatime,compress=zstd 0 0
UUID=${btrfs_uuid} 	/var/log 	btrfs   subvol=arch-i3_log,noatime,compress=zstd 0 0
UUID=${btrfs_uuid} 	/_btrbk_snap btrfs 	subvol=btrbk_snap,noatime,compress=zstd 0 0
UUID=${btrfs_uuid} 	/share 		btrfs   subvol=share,noatime,compress=zstd 0 0
tmpfs 				/tmp 		tmpfs 	defaults,noatime,mode=1777 0 0
FSTAB
    
# Array containing paths to resources
declare -A resources
resources[ssh]=".ssh"
resources[librewolf]=".librewolf"
resources[applications]="Applications"
resources[documents]="Documents"
resources[downloads]="Downloads"
resources[git]="Git"
resources[music]="Music"
resources[pictures]="Pictures"
resources[sync]="Sync"
resources[videos]="Videos"
resources[signal]=".config/Signal"
resources[joplin]=".config/Joplin"
resources[joplin-desktop]=".config/joplin-desktop"
resources[onlyoffice-config]=".config/onlyoffice"
resources[onlyoffice-local]=".local/share/onlyoffice"
resources[syncthing]=".local/state/syncthing"
    
# Build the fstab entries
> ~/temp_fstab.txt
for resource in "${!resources[@]}"; do
  mount_point="/home/${username}/${resources[$resource]}"
  subvol="/share/${resources[$resource]}"
  printf 'UUID=%s %s btrfs subvol=%s,noatime,compress=zstd 0 0\n' "${btrfs_uuid}" "${mount_point}" "${subvol}" >> ~/temp_fstab.txt
done
    
# Append to /etc/fstab
echo "Updating /etc/fstab..."
if ! diff -q /etc/fstab ~/temp_fstab.txt >/dev/null; then
  cat ~/temp_fstab.txt >> /etc/fstab
  rm ~/temp_fstab.txt
fi

# Set the time zone
echo "Setting time zone..."
ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
    
# Run hwclock to generate /etc/adjtime
hwclock --systohc
    
# Add locale to /etc/locale.gen
echo "Adding locale..."
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
    
# Generate the locales
echo "Generating locales..."
locale-gen
    
# Create the locale.conf file, and set the LANG variable:
echo "LANG=en_US.UTF-8" > /etc/locale.conf
    
# Create the hostname file:
echo "Creating the hostname file..."
echo "${machine_hostname}" > /etc/hostname
hostnamectl set-hostname "${machine_hostname}" --static

# Define cryptdevice UUID for encrypted installations
if [[ "$luks_install" == "y" ]]; then
    cryptdevice_uuid=$(blkid -s UUID -o value "${btrfs_partition}")
fi

# Add encrypt hook to mkinitcpio.conf for LUKS install
if [[ "$luks_install" == "y" ]]; then
    echo "Adding encrypt hooks to mkinitcpio.conf..."
    sed -i "s/udev/systemd/" /etc/mkinitcpio.conf || error "Failed to replace udev with systemd in mkinitcpio.conf"
    sed -i "s/consolefont/sd-vconsole/" /etc/mkinitcpio.conf || error "Failed to replace consolefont with sd-vconsole in mkinitcpio.conf"
    sed -i "/^HOOKS=/ s/block/& sd-encrypt/" /etc/mkinitcpio.conf || error "Failed to insert hook into mkinitcpio.conf"
    # Add fido2 option to initramfs
    cat <<EOF >> /etc/crypttab.initramfs
root UUID=${cryptdevice_uuid} - fido2-device=auto
EOF
    echo "Regenerating initramfs images..."
    mkinitcpio -P || error "Failed to regenerate initramfs images."
fi

# Set root password
echo "Setting root password..."
echo "root:${password}" | chpasswd || error "Failed to set root password."
    
# Create a new user
echo "Creating a new user..."
useradd -m -G wheel "${username}" || error "Failed to create user."
    
# Set password for the new user
echo "Setting password for the new user..."
echo "${username}:${password}" | chpasswd || error "Failed to set user password."

# Set user ownership for share
chown -R "${username}:${username}" /share

# Create user directories
mkdir -p "/home/${username}"/{.librewolf,.ssh,Documents,Downloads,Git,Music,Pictures,Sync,Videos}
mkdir -p "/home/${username}"/.config/{Signal,Joplin,joplin-desktop,onlyoffice}
mkdir -p "/home/${username}/.local/share/onlyoffice"
mkdir -p "/home/${username}/.local/state/syncthing"

# Create btrbk directories
mkdir -p /_btrbk_snap/{Documents,Music,Pictures,Sync,Videos,arch-i3}

# Install rEFInd and configure the boot stanza
refind-install
if [[ "$luks_install" == "y" ]]; then
    # Generate rEFInd boot stanza
    echo "Generating a rEFInd boot stanza..."
    cat <<REFIND >> /efi/EFI/refind/refind.conf
menuentry "Arch i3" {
    icon 	/EFI/refind/arch-i3.png
    volume 	boot
    loader 	/arch-i3_boot/vmlinuz-linux
    initrd 	/arch-i3_boot/initramfs-linux.img
    graphics on
    options "rd.luks.name=${cryptdevice_uuid}=root root=/dev/mapper/root rw rootflags=subvol=arch-i3"
    submenuentry "Fallback initramfs" {
        initrd	/arch-i3_boot/initramfs-linux-fallback.img
    }
    submenuentry "LTS kernel" {
        loader	/arch-i3_boot/vmlinuz-linux-lts
        initrd	/arch-i3_boot/initramfs-linux-lts.img
    }
    submenuentry "LTS fallback" {
        loader	/arch-i3_boot/vmlinuz-linux-lts
        initrd	/arch-i3_boot/initramfs-linux-lts-fallback.img
    }		
}

include additional_stanzas.conf

include themes/refind-theme-regular/theme.conf

REFIND
else
    # Generate rEFInd boot stanza
    echo "Generating a rEFInd boot stanza..."
    cat <<REFIND >> /efi/EFI/refind/refind.conf
menuentry "Arch-i3" {
    icon 	/EFI/refind/arch-i3.png
    volume 	Btrfs
    loader 	/arch-i3/boot/vmlinuz-linux
    initrd 	/arch-i3/boot/initramfs-linux.img
    graphics on
    options "root=UUID=${btrfs_uuid} rw rootflags=subvol=arch-i3"
    submenuentry "Fallback initramfs" {
        initrd	/arch-i3/boot/initramfs-linux-fallback.img
    }
    submenuentry "LTS kernel" {
        loader	/arch-i3/boot/vmlinuz-linux-lts
        initrd	/arch-i3/boot/initramfs-linux-lts.img
    }
    submenuentry "LTS fallback" {
        loader	/arch-i3/boot/vmlinuz-linux-lts
        initrd	/arch-i3/boot/initramfs-linux-lts-fallback.img
    }		
}

include additional_stanzas.conf

include themes/refind-theme-regular/theme.conf

REFIND
fi

# Create additional_stanzas.conf
touch /efi/EFI/refind/additional_stanzas.conf || error "Failed to create additional_stanzas.conf."

# Configure login from TTY1
echo "Configuring login from TTY..."
cat <<BASHRC >> "/home/${username}/.bashrc"
# Log in from TTY1
if [ "\$(tty)" = "/dev/tty1" ]; then
    startx
fi
BASHRC
# Set up drop-in file for Getty
mkdir -p /etc/systemd/system/getty@tty1.service.d/ || error "Failed to create drop-in directory for Getty."
cat <<GETTY >> /etc/systemd/system/getty@tty1.service.d/skip-username.conf
[Service]
ExecStart=
ExecStart=-/sbin/agetty -o '-p -- '"${username}"'' --noclear --skip-login - \$TERM
GETTY
# Enable the getty service
systemctl enable getty@tty1.service || error "Failed to enable getty@tty1.service."

# Clone the installation repo
echo "Cloning installation repo..."
git clone https://gitlab.com/BluishHumility/arch-i3.git || error "Failed to clone installation repo."

# Clone the common-configs repo
echo "Cloning common-configs repo..."
git clone https://gitlab.com/BluishHumility/common-configs.git || error "Failed to clone common-configs repo."

# Add the MIT mirror to /etc/pacman.conf
echo "Adding MIT mirror to /etc/pacman.conf..."
server_url="https://mirrors.mit.edu/archlinux/\$repo/os/\$arch"
sed -i "/^\s*Include\s*=/ { s|^\s*Include\s*=.*|Server = $server_url\n&|; }" /etc/pacman.conf
# Also color and ILoveCandy
sed -i 's|#Color|Color\nILoveCandy|' /etc/pacman.conf

# Install chaotic packages
echo "Adding Chaotic repo..."
pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com || error "Failed to receive Chaotic repo key."
pacman-key --lsign-key 3056513887B78AEB || error "Failed to locally sign Chaotic repo key."
pacman -U --noconfirm 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
echo -e "\n[chaotic-aur]\nInclude = /etc/pacman.d/chaotic-mirrorlist" >> /etc/pacman.conf
pacman -Sy || error "Failed to resync mirrors."
echo "Installing Chaotic AUR packages..."
pacman -S --noconfirm --noprogressbar --needed --disable-download-timeout $(< arch-i3/chaotic-packages.list)

# Install Librewolf AppImage
echo "Installing Librewolf AppImage..."
latest_release=$(curl -s "https://gitlab.com/api/v4/projects/24386000/releases" | jq -r '.[0].tag_name' | sed 's/^v//' ) || error "Failed to curl latest LibreWolf release info."
curl -L -o "/share/Applications/LibreWolf.AppImage" "https://gitlab.com/api/v4/projects/24386000/packages/generic/librewolf/${latest_release}/LibreWolf.x86_64.AppImage" || error "LibreWolf download failed."
chmod +x "/share/Applications/LibreWolf.AppImage"

# Install Joplin AppImage
echo "Installing Joplin AppImage..."
latest_release=$(curl -s "https://api.github.com/repos/laurent22/joplin/releases/latest" | jq -r '.tag_name' | sed 's/v//') || error "Failed to curl latest Joplin release info."
curl -L -o "/share/Applications/Joplin.AppImage" "https://github.com/laurent22/joplin/releases/download/v${latest_release}/Joplin-${latest_release}.AppImage" || error "Joplin download failed."
chmod +x "/share/Applications/Joplin.AppImage"

# Install OnlyOffice AppImage
echo "Installing OnlyOffice AppImage..."
latest_release=$(curl -s "https://api.github.com/repos/ONLYOFFICE/appimage-desktopeditors/releases/latest" | jq -r '.tag_name' | sed 's/v//') || error "Failed to curl latest OnlyOffice release info."
curl -L -o "/share/Applications/OnlyOffice.AppImage" "https://github.com/ONLYOFFICE/appimage-desktopeditors/releases/download/v${latest_release}/DesktopEditors-x86_64.AppImage" || error "OnlyOffice download failed."
chmod +x "/share/Applications/OnlyOffice.AppImage"

# # Install GIMP AppImage
# echo "Installing GIMP AppImage..."
# download_url=$(curl -s https://api.github.com/repos/ivan-hc/GIMP-appimage/releases/tags/continuous-stable | grep -o 'https://github.com/ivan-hc/GIMP-appimage/releases/download/continuous-stable/GNU-Image-Manipulation-Program_[^"]*\.AppImage' | head -n 1)
# curl -L -o "/share/Applications/GIMP.AppImage" "$download_url"
# chmod +x "/share/Applications/GIMP.AppImage"

# Install Krita AppImage
echo "Installing Krita AppImage..."
base_url="https://download.kde.org/stable/krita/"
latest_release=$(curl -s "$base_url" | grep -oP 'href="\K[0-9]+\.[0-9]+\.[0-9]+(?=/)' | sort -V | tail -n 1) || error "Failed to curl latest Krita release info."
curl -L -o "/share/Applications/Krita.AppImage" "${base_url}${latest_release}/krita-${latest_release}-x86_64.appimage" || error "Krita download failed."
chmod +x "/share/Applications/Krita.AppImage"

# Install darktable AppImage
echo "Installing darktable AppImage..."
latest_release=$(curl -s "https://api.github.com/repos/darktable-org/darktable/releases/latest" | jq -r '.tag_name' | sed 's/release-//' ) || error "Failed to curl latest darktable release info."
curl -L -o "/share/Applications/darktable.AppImage" "https://github.com/darktable-org/darktable/releases/download/release-${latest_release}/darktable-${latest_release}-x86_64.AppImage" || error "darktable download failed."
chmod +x "/share/Applications/darktable.AppImage"

# Install Upscayl AppImage
echo "Installing Upscayl AppImage..."
latest_release=$(curl -s "https://api.github.com/repos/upscayl/upscayl/releases/latest" | jq -r '.tag_name' | sed 's/v//' ) || error "Failed to curl latest Upscayl release info."
curl -L -o "/share/Applications/Upscayl.AppImage" "https://github.com/upscayl/upscayl/releases/download/v${latest_release}/upscayl-${latest_release}-linux.AppImage" || error "Upscayl download failed."
chmod +x "/share/Applications/darktable.AppImage"

# Set up AppImage symlinks
echo "Setting up AppImage symlinks..."
ln -s "/home/${username}/Applications/LibreWolf.AppImage" /usr/local/bin/librewolf
ln -s "/home/${username}/Applications/Joplin.AppImage" /usr/local/bin/joplin
ln -s "/home/${username}/Applications/OnlyOffice.AppImage" /usr/local/bin/onlyoffice
#ln -s "/home/${username}/Applications/GIMP.AppImage" /usr/local/bin/gimp
ln -s "/home/${username}/Applications/Krita.AppImage" /usr/local/bin/krita
ln -s "/home/${username}/Applications/darktable.AppImage" /usr/local/bin/darktable
ln -s "/home/${username}/Applications/Upscayl.AppImage" /usr/local/bin/upscayl

# Deploy user configs
echo "Deploying user configs..."
rsync -a arch-i3/home_config/ "/home/${username}/" || error "Failed to deploy home configs."
rsync -a common-configs/.config "/home/${username}/" || error "Failed to deploy user configs."
rsync -a arch-i3/.config "/home/${username}/" || error "Failed to deploy user configs."
rsync -a common-configs/.local "/home/${username}/" || error "Failed to deploy user configs."
rsync -a arch-i3/.local "/home/${username}/" || error "Failed to deploy user configs."
# Restore user ownership
chown -R "${username}:${username}" "/home/${username}"

# A few configs I like root to have
root_configs=(
	"fish/"
	"Kvantum/"
	"lf/"
	"micro/"
)
# Copy the configs over to the root user
mkdir -p /root/.config/
for root_config in "${root_configs[@]}"; do
    cp -R "/home/${username}/.config/$root_config" /root/.config/
done
# The ownership should already be root, but just in case
chown -R root:root /root/.config/

# Deploy system configs
echo "Deploying system configs..."
# Copy system files and restore root ownership
rsync -a --chown=root:root common-configs/etc/ /etc/ || error "Failed to deploy system configs."
rsync -a --chown=root:root arch-i3/etc/ /etc/ || error "Failed to deploy system configs."
rsync -a --chown=root:root common-configs/usr/ /usr/ || error "Failed to deploy /usr/local/bin scripts."

# Add rEFInd theme
echo "Adding rEFInd theme and custom icons..."
mkdir -p /efi/EFI/refind/themes
cp -r arch-i3/refind-theme-regular /efi/EFI/refind/themes/ || error "Failed to add rEFInd theme."

# Add custom rEFInd icons
cp arch-i3/png/* /efi/EFI/refind/ || error "Failed to copy rEFInd icons."

# Create Snapper configs
echo "Creating Snapper config for root..."
snapper --no-dbus create-config --template custom_root / || error "Failed to create Snapper config for root."
# If there is a boot partition, create a Snapper config for it
if [[ "$luks_install" == "y" ]]; then
    echo "Creating Snapper config for boot..."
    snapper --no-dbus -c boot create-config --template custom_root /boot || error "Failed to create Snapper config for root."
    # Add boot config to snap-pac.ini
    echo "Adding boot config to snap-pac.ini..."
    cat <<EOF >> /etc/snap-pac.ini

[boot]
snapshot = True
EOF
fi

# Correct Whoogle directory ownership
echo "Correcting Whoogle directory ownership..."
chown -R whoogle:whoogle /opt/whoogle-search/ || error "Failed to correct Whoogle directory ownership."

# Enable systemd services
echo "Enabling systemd services..."
systemctl enable NetworkManager.service || error "Failed to enable NetworkManager."
systemctl enable btrbk-snapshot.timer || error "Failed to enable Btrbk Snapshot timer."
systemctl enable snapper-cleanup.timer || error "Failed to enable Snapper cleanup timer."
systemctl enable tailscaled.service || error "Failed to enable Tailscale service."
systemctl enable whoogle.service || error "Failed to enable Whoogle service."

# Remove the installation and common-configs repos
echo "Removing installation and common-configs repos..."
rm -r arch-i3 || error "Failed to remove installation repo."
rm -r common-configs || error "Failed to remove common-configs repo."

# Exit the chroot
echo "Exiting the chroot..."
exit
