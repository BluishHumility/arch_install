## What is this?

This is a repo with some scripts and configuration files for installing Arch Linux.

## How does it work?

Boot into the Arch live environment and pull down the `install.sh` script with curl:

```
curl https://gitlab.com/BluishHumility/arch-i3/-/raw/main/install.sh > install.sh
```

Make the script executable and run it.

## Why did you make this?

A few reasons:

* Instead of installing Arch Linux once and then forgetting half the things I did the next time I want to do it, writing and maintaining an installation script like this is somewhat self-documenting. In the future, if I wish to refer to how I have configured something I can simply look in the repo and see how it has been set up.
* It takes more time up front to create an installation script like this than to just do the installation, but once the script is done you can essentially "skip" the lengthy process of installing and configuring Arch Linux. This is handy for quickly spinning up an install for testing or other purposes.
* This has been a fun way to learn and practice some basic shell scripting, and to learn more about Arch Linux in general.

## Can I use it?

Technically yes, however it is unlikely you would want exactly the setup that I have configured here. This is a highly opinionated i3 installation with a setup that is very specific to my preference and use case.

I have made it an open repo anyway in case anyone would like to take a look, and perhaps incorporate some of the ideas here into their own installation process.
