#!/bin/bash

# ❯ cvt 1500 1000
# # 1504x1000 59.85 Hz (CVT) hsync: 62.12 kHz; pclk: 124.25 MHz
# Modeline "1504x1000_60.00"  124.25  1504 1600 1752 2000  1000 1003 1013 1038 -hsync +vsync
# ❯ cvt 2160 1440
# # 2160x1440 59.95 Hz (CVT) hsync: 89.50 kHz; pclk: 263.50 MHz
# Modeline "2160x1440_60.00"  263.50  2160 2320 2552 2944  1440 1443 1453 1493 -hsync +vsync

# Check if the desk monitors are connected
if xrandr | grep -q "DP-4-1 connected" && xrandr | grep -q "DP-4-2 connected"; then
  # Add the custom resolution, set up the displays
  xrandr --newmode "1504x1000_60.00" 124.25 1504 1600 1752 2000 1000 1003 1013 1038 -hsync +vsync
  xrandr --addmode eDP-1 1504x1000_60.00
  xrandr --output eDP-1 --mode 1504x1000_60.00 --pos 0x200
  xrandr --output DP-4-1 --mode 2560x1440 --pos 1504x0
  xrandr --output DP-4-2 --mode 1920x1080 --pos 4064x0 --rotate left
else
  xrandr --newmode "2160x1440_60.00" 263.50 2160 2320 2552 2944 1440 1443 1453 1493 -hsync +vsync
  xrandr --addmode eDP-1 2160x1440_60.00
  xrandr --output eDP-1 --mode 2160x1440_60.00
fi
