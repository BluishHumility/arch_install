#!/bin/bash

# Define the options for the rofi menu
options="Log out\nSuspend\nReboot\nPower off"

# Display the rofi menu and get the user's selection
selected_option=$(echo -e "$options" | rofi -dmenu -p "Select an option:" -config "~/.config/rofi/power_menu.rasi")

# Execute the command based on the selected option
case $selected_option in
    "Log out")
        confirmation=$(echo -e "Yes\nNo" | rofi -dmenu -p "Are you sure you want to log out?" -config "~/.config/rofi/power_menu.rasi")
        if [ "$confirmation" = "Yes" ]; then
            i3-msg exit
        fi
        ;;
    "Suspend")
        confirmation=$(echo -e "Yes\nNo" | rofi -dmenu -p "Are you sure you want to suspend?" -config "~/.config/rofi/power_menu.rasi")
        if [ "$confirmation" = "Yes" ]; then
            systemctl suspend
        fi
        ;;
    "Reboot")
        confirmation=$(echo -e "Yes\nNo" | rofi -dmenu -p "Are you sure you want to reboot?" -config "~/.config/rofi/power_menu.rasi")
        if [ "$confirmation" = "Yes" ]; then
            systemctl reboot
        fi
        ;;
    "Power off")
        confirmation=$(echo -e "Yes\nNo" | rofi -dmenu -p "Are you sure you want to power off?" -config "~/.config/rofi/power_menu.rasi")
        if [ "$confirmation" = "Yes" ]; then
            systemctl poweroff
        fi
        ;;
    *)
        echo "Invalid option"
        ;;
esac
