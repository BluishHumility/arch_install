#!/bin/bash

# Create a log file
LOG_FILE="installation_log.txt"

# Function to print error messages
error() {
    echo "Error: $1" >&2
    echo "Error: $1" >> "$LOG_FILE"
    exit 1
}

# Redirect stdout and stderr to the log file
exec > >(tee -a "$LOG_FILE")
exec 2>&1

# Check if script is run as root
if [ "$EUID" -ne 0 ]; then
    error "Please run this script as root."
fi

# Establish if encryption will be set up
while true; do
    read -p "Set up encryption? (y/n): " luks_install
    if [[ "$luks_install" == "y" || "$luks_install" == "n" ]]; then
        break  # Exit the loop if the input is "y" or "n"
    else
        echo "Invalid input. Please enter 'y' or 'n'."
    fi
done

# Confirm encryption choice
if [[ "$luks_install" == "y" ]]; then
    echo "Proceeding with encrypted install."
else
    echo "Proceeding with non-encrypted install."
fi

# Define the username
read -p "Enter username: " username

# Define user password
while true; do
    # Get the password
    read -rsp "Enter password: " password
    echo

    # Confirmation password
    read -rsp "Confirm password: " password_confirm
    echo

    # Check if passwords match
    if [ "$password" = "$password_confirm" ]; then
        break  # Passwords match, break the loop
    else
        echo "The confirmation password did not match--try again."
    fi
done

# Define the hostname
read -p "Enter hostname (computer name): " hostname

# List available disks
lsblk -o name,type,fstype,label,size

# Ask the user to select a disk
read -p "Enter the disk to use (e.g., sda): " disk_selection
disk="/dev/$disk_selection"

# Verify disk selection
read -p "Are you sure you want to use $disk for installation? (y/n): " choice
if [ "$choice" != "y" ]; then
    error "Installation aborted."
fi

# Format disk with GPT
echo "Formatting $disk with GPT..."
parted -s "$disk" mklabel gpt || error "Failed to format disk with GPT."

# Create EFI partition
echo "Creating EFI partition..."
parted -s "$disk" mkpart primary fat32 0% 100MiB name 1 EFI || error "Failed to create EFI partition."
parted -s "$disk" set 1 boot on || error "Failed to set boot flag on EFI partition."
parted -s "$disk" set 1 esp on || error "Failed to set esp flag on EFI partition."

# Create the Btrfs partition. For a LUKS install, create the boot partition as well.
if [[ "$luks_install" == "y" ]]; then
    # Create boot partition
    echo "Creating boot partition..."
    parted -s "$disk" mkpart primary btrfs 100MiB 8GB name 2 boot || error "Failed to create boot partition."
    # Create Btrfs partition
    echo "Creating Btrfs partition..."
    parted -s "$disk" mkpart primary btrfs 8GB 100% name 3 Btrfs || error "Failed to create Btrfs partition."
else
    # Create Btrfs partition
    echo "Creating Btrfs partition..."
    parted -s "$disk" mkpart primary btrfs 100MiB 100% name 2 Btrfs || error "Failed to create Btrfs partition."
fi

# Inform user of completion
echo "Disk setup completed."

# Define EFI partition
if [[ "$disk" == *"nvme"* ]]; then
    efi_partition="${disk}p1"
else
    efi_partition="${disk}1"
fi

# Define the Btrfs partition. For a LUKS install, define the boot partition as well.
if [[ "$luks_install" == "y" ]]; then
    # Define boot partition
    if [[ "$disk" == *"nvme"* ]]; then
        boot_partition="${disk}p2"
    else
        boot_partition="${disk}2"
    fi
    echo "Boot partition = ${boot_partition}"
    # Define Btrfs partition
    if [[ "$disk" == *"nvme"* ]]; then
        btrfs_partition="${disk}p3"
    else
        btrfs_partition="${disk}3"
    fi
    echo "Btrfs partition = ${btrfs_partition}"
else
    # Define Btrfs partition
    if [[ "$disk" == *"nvme"* ]]; then
        btrfs_partition="${disk}p2"
    else
        btrfs_partition="${disk}2"
    fi
    echo "Btrfs partition = ${btrfs_partition}"
fi

# Format EFI partition
echo "Formatting EFI partition..."
mkfs.fat -F32 -n EFI "${efi_partition}" || error "Failed to format EFI partition as FAT32."

# Format and mount the Btrfs partition. For a LUKS install, also format the boot partition and apply encryption.
if [[ "$luks_install" == "y" ]]; then
    # Format boot partition
    echo "Formatting boot partition..."
    mkfs.btrfs -f "${boot_partition}" || error "Failed to format boot partition as Btrfs."
    # Format the Btrfs partition and encrypt with LUKS
    echo "Encrypting Btrfs partition with LUKS..."
    cryptsetup -y -v luksFormat --hash=sha256 --pbkdf=pbkdf2 "${btrfs_partition}" || error "Failed to format Btrfs partition as LUKS."
    cryptsetup open "${btrfs_partition}" root || error "Failed to open encrypted Btrfs partition."
    mkfs.btrfs -f /dev/mapper/root || error "Failed to format encrypted Btrfs partition as Btrfs."
    # Mount the Btrfs partition
    echo "Mounting Btrfs partition..."
    mount -t btrfs /dev/mapper/root /mnt || error "Failed to mount encrypted Btrfs partition."
else
    # Format Btrfs partition
    echo "Formatting Btrfs partition..."
    mkfs.btrfs -f "${btrfs_partition}" || error "Failed to format Btrfs partition as Btrfs."
    # Mount Btrfs partition
    echo "Mounting Btrfs partition..."
    mount "${btrfs_partition}" /mnt || error "Failed to mount Btrfs partition."
fi

# Create top-level subvolumes
echo "Creating top-level subvolumes..."
btrfs subvolume create /mnt/arch-i3 || error "Failed to create arch-i3 subvolume"
btrfs subvolume create /mnt/arch-i3_cache || error "Failed to create arch-i3_cache subvolume"
btrfs subvolume create /mnt/arch-i3_home || error "Failed to create arch-i3_home subvolume"
btrfs subvolume create /mnt/arch-i3_log || error "Failed to create arch-i3_log subvolume"
btrfs subvolume create /mnt/btrbk_snap || error "Failed to create btrbk_snap subvolume"
btrfs subvolume create /mnt/share || error "Failed to create share subvolume"

# Mount filesystem root
echo "Mounting filesystem root..."
umount /mnt
if [[ "$luks_install" == "y" ]]; then
    mount -t btrfs -o noatime,compress=zstd,subvol=arch-i3 "/dev/mapper/root" /mnt || error "Failed to mount root subvolume."
else
    mount -o noatime,compress=zstd,subvol=arch-i3 "${btrfs_partition}" /mnt || error "Failed to mount root subvolume."
fi

# Create needed directories
echo "Creating new directories..."
mkdir -p /mnt/{_btrbk_snap,boot,efi,home,share,var/{cache,log}}

# Mount top-level subvolumes
if [[ "$luks_install" == "y" ]]; then
    mount -t btrfs -o noatime,compress=zstd,subvol=arch-i3_cache "/dev/mapper/root" /mnt/var/cache || error "Failed to mount arch-i3_cache subvolume."
    mount -t btrfs -o noatime,compress=zstd,subvol=arch-i3_home "/dev/mapper/root" /mnt/home || error "Failed to mount arch-i3_home subvolume."
    mount -t btrfs -o noatime,compress=zstd,subvol=arch-i3_log "/dev/mapper/root" /mnt/var/log || error "Failed to mount arch-i3_log subvolume."
    mount -t btrfs -o noatime,compress=zstd,subvol=btrbk_snap "/dev/mapper/root" /mnt/_btrbk_snap || error "Failed to mount btrbk_snap subvolume."
    mount -t btrfs -o noatime,compress=zstd,subvol=share "/dev/mapper/root" /mnt/share || error "Failed to mount share subvolume."
else
    mount -o noatime,compress=zstd,subvol=arch-i3_cache "${btrfs_partition}" /mnt/var/cache || error "Failed to mount arch-i3_cache subvolume."
    mount -o noatime,compress=zstd,subvol=arch-i3_home "${btrfs_partition}" /mnt/home || error "Failed to mount arch-i3_home subvolume."
    mount -o noatime,compress=zstd,subvol=arch-i3_log "${btrfs_partition}" /mnt/var/log || error "Failed to mount arch-i3_log subvolume."
    mount -o noatime,compress=zstd,subvol=btrbk_snap "${btrfs_partition}" /mnt/_btrbk_snap || error "Failed to mount btrbk_snap subvolume."
    mount -o noatime,compress=zstd,subvol=share "${btrfs_partition}" /mnt/share || error "Failed to mount share subvolume."
fi

# Create nested subvolumes
echo "Creating nested subvolumes..."
btrfs subvolume create /mnt/share/.ssh || error "Failed to create .ssh subvolume."
btrfs subvolume create /mnt/share/.librewolf || error "Failed to create .librewolf subvolume."
btrfs subvolume create /mnt/share/Documents || error "Failed to create Documents subvolume."
btrfs subvolume create /mnt/share/Downloads || error "Failed to create Downloads subvolume."
btrfs subvolume create /mnt/share/Git || error "Failed to create Git subvolume."
btrfs subvolume create /mnt/share/Applications || error "Failed to create Applications subvolume."
btrfs subvolume create /mnt/share/Music || error "Failed to create Music subvolume."
btrfs subvolume create /mnt/share/Pictures || error "Failed to create Pictures subvolume."
btrfs subvolume create /mnt/share/Sync || error "Failed to create Sync subvolume."
btrfs subvolume create /mnt/share/Videos || error "Failed to create Videos subvolume."
# Create the config directory on the shared subvolume
mkdir -p /mnt/share/.config
btrfs subvolume create /mnt/share/.config/Signal || error "Failed to create Signal subvolume."
btrfs subvolume create /mnt/share/.config/Joplin || error "Failed to create Joplin subvolume."
btrfs subvolume create /mnt/share/.config/joplin-desktop || error "Failed to create joplin-desktop subvolume."
btrfs subvolume create /mnt/share/.config/onlyoffice || error "Failed to create onlyoffice subvolume."
mkdir -p /mnt/share/.local/{share,state}
btrfs subvolume create /mnt/share/.local/share/onlyoffice || error "Failed to create onlyoffice subvolume."
btrfs subvolume create /mnt/share/.local/state/syncthing || error "Failed to create syncthing subvolume."

# Create a boot subvolume if it is a LUKS install
if [[ "$luks_install" == "y" ]]; then
    # Mount the boot partition
    echo "Mounting boot partition..."
    mount "${boot_partition}" /mnt/boot || error "Failed to mount boot partition."
    # Create boot subvolume
    echo "Creating boot subvolume..."
    btrfs subvolume create /mnt/boot/arch-i3_boot || error "Failed to create arch-i3_boot subvolume"    
    # Mount boot subvolume
    echo "Mounting boot subvolume..."
    umount /mnt/boot
    mount -o noatime,compress=zstd,subvol=arch-i3_boot "${boot_partition}" /mnt/boot || error "Failed to mount boot subvolume."
fi

# Mount EFI partition
echo "Mounting EFI partition..."
mount "${efi_partition}" /mnt/efi || error "Failed to mount EFI partition."

# Pull down the package list
echo "Retrieving the package list from remote target..."
curl -sSL https://gitlab.com/BluishHumility/arch-i3/-/raw/main/packages.list -o packages.list

# Check CPU vendor and append packages to packages.list
if lscpu | grep -qi "Intel"; then
    echo "Intel CPU detected, installing packages..."
    echo "intel-ucode" >> packages.list
    echo "libva-intel-driver" >> packages.list
elif lscpu | grep -qi "AMD"; then
    echo "AMD CPU detected, installing packages..."
    echo "amd-ucode" >> packages.list
    echo "vulkan-radeon" >> packages.list
    echo "xf86-video-amdgpu" >> packages.list
    echo "libva-mesa-driver" >> packages.list
else
    echo "CPU vendor could not be determined, installing both microcode packages..."
    echo "intel-ucode" >> packages.list
    echo "amd-ucode" >> packages.list
fi

# Install packages
echo "Installing packages..."
pacstrap -K /mnt $(< packages.list)

# Pull down the arch-chroot script
echo "Retrieving the arch-chroot script from remote target..."
curl -sSL https://gitlab.com/BluishHumility/arch-i3/-/raw/main/arch-chroot.sh -o /mnt/arch-chroot.sh

# Prepare the arch-chroot script
echo "Preparing the arch-chroot script..."
chmod +x /mnt/arch-chroot.sh
sed -i -e "s|luks_install_placeholder|\"${luks_install}\"|" \
       -e "s|username_placeholder|\"${username}\"|" \
       -e "s|password_placeholder|\"${password}\"|" \
       -e "s|hostname_placeholder|\"${hostname}\"|" \
       -e "s|btrfs_partition_placeholder|\"${btrfs_partition}\"|" /mnt/arch-chroot.sh

# Arch-chroot and run the script
echo "Arch-chroot into the new system..."
arch-chroot /mnt /bin/bash -c "/arch-chroot.sh"

# Delete the arch-chroot script
rm /mnt/arch-chroot.sh

echo "Installation complete."
